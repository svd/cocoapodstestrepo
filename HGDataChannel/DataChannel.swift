//
//  DataChannel.swift
//  HGDataChannel
//
//  Created by Silviu Deac on 17/12/2016.
//  Copyright © 2016 Silviu Deac. All rights reserved.
//

import UIKit
import SocketIO

class DataChannel: NSObject {

    var socket: SocketIOClient?
    
    init(endpoint: URL) {
        
        var config = SocketIOClientConfiguration()
        let optionWebsockets = SocketIOClientOption.forceWebsockets(true)
        let optionReconnectAttempts = SocketIOClientOption.reconnectAttempts(1)
        
        config.insert(optionWebsockets)
        config.insert(optionReconnectAttempts)
        
        socket = SocketIOClient(socketURL: endpoint, config: config)
    }
    
    func connect() {
        
        guard let websocket = socket else {
            print("Socket is nil")
            return
        }
        
        websocket.connect()
    }
    
    func disconnect() {
        
        guard let websocket = socket else {
            print("Socket is nil")
            return
        }
        
        websocket.disconnect()
    }
    
}
